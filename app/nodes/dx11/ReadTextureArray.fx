Texture2DArray texArray <string uiname="Texture Array";>;

SamplerState g_samLinear : IMMUTABLE
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
	AddressW = Wrap;
};
 
cbuffer cbPerObj : register(b0)
{
	int zSlice <string uiname="Slice Z"; >;
};

struct vsInput
{
	float4 pos : POSITION;
	float4 uv : TEXCOORD0;
};

struct psInput
{
    float4 pos: SV_POSITION;
    float4 uv: TEXCOORD0;
};

psInput VS(vsInput input)
{
	return input;
}


float4 PS(psInput input): SV_Target
{
	float3 uvs = float3(input.uv.xy, zSlice);	
    float4 col = texArray.Sample(g_samLinear,uvs);
    return col;
}

technique10 Constant
{
	pass P0
	{
		SetVertexShader( CompileShader( vs_4_0, VS() ) );
		SetPixelShader( CompileShader( ps_4_0, PS() ) );
	}
}




