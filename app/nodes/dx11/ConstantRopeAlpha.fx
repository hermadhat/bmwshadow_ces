//@author: vux
//@help: standard constant shader
//@tags: color
//@credits: 

struct vsInputTextured
{
    float4 posObject : POSITION;
	float4 uv: TEXCOORD0;
};

struct psInputTextured
{
    float4 posScreen : SV_Position;
    float4 uv: TEXCOORD0;
};

cbuffer cbPerDraw : register(b0)
{
	float4x4 tVP : VIEWPROJECTION;
};

cbuffer cbPerObj : register( b1 )
{
	float4x4 tW : WORLD;
	float Alpha <float uimin=0.0; float uimax=1.0;> = 1; 
	float4 cAmb <bool color=true;String uiname="Color";> = { 1.0f,1.0f,1.0f,1.0f };
	float4x4 tColor <string uiname="Color Transform";>;
	float powerv = 1.0f;
	float scalev = 1.0f;
	float poweru = 1.0f;
	float scaleu = 1.0f;
};

cbuffer cbTextureData : register(b2)
{
	float4x4 tTex <string uiname="Texture Transform"; bool uvspace=true; >;
};

psInputTextured VS_Textured(vsInputTextured input)
{
	psInputTextured output;
	output.posScreen = mul(input.posObject,mul(tW,tVP));
	output.uv = mul(input.uv, tTex);
	return output;
}


float4 PS_Textured(psInputTextured input): SV_Target
{
    float4 col = cAmb;
	float u = input.uv.x;
	float v = input.uv.y;
	
	v = pow(1.0f - abs(v-0.5f)*2.0f, powerv)*scalev;
	u = pow(1.0f - abs(u-0.5f)*2.0f, poweru)*scaleu;
	
	col *= v;
	col *= u;
	col = mul(col, tColor);
	col.a *= Alpha;
    return col;
}

technique11 Constant <string noTexCdFallback="ConstantNoTexture"; >
{
	pass P0
	{
		SetVertexShader( CompileShader( vs_4_0, VS_Textured() ) );
		SetPixelShader( CompileShader( ps_4_0, PS_Textured() ) );
	}
}


