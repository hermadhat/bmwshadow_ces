Texture2D texture2d <string uiname="Texture";>;
int sliceID;

SamplerState g_samLinear <string uiname="Sampler State";>
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
};

struct vsInput
{
	float4 pos : POSITION;
	float4 uv : TEXCOORD0;
};

struct gsInput
{
	float4 pos : POSITION;
	float4 uv : TEXCOORD0;
};
struct psInput
{
    float4 pos: SV_Position;
    float4 uv : TEXCOORD0;
	uint rt : SV_RenderTargetArrayIndex;
};
gsInput VS(vsInput input)
{
	gsInput output;
	output.pos = input.pos;
	output.uv = input.uv;
    return output;
}
[maxvertexcount(3)]
void GS(triangle gsInput input[3], inout TriangleStream<psInput> gsout)
{
	psInput output;
	[unroll]
	for (int i = 0; i < 3; i++)
	{
		output.pos = input[i].pos;
		output.uv = input[i].uv;
		output.rt = sliceID;
		gsout.Append(output);
	}
	gsout.RestartStrip();
}


float4 PS(psInput input): SV_Target
{
    return texture2d.Sample(g_samLinear,input.uv.xy);
}

technique10 Render
{
	pass P0
	{
		SetVertexShader( CompileShader( vs_4_0, VS() ) );
		SetGeometryShader (CompileShader(gs_4_0, GS()));
		SetPixelShader( CompileShader( ps_4_0, PS() ) );
		
	}
}




