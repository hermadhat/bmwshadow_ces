#region usings
using System;
using System.ComponentModel.Composition;

using VVVV.PluginInterfaces.V1;
using VVVV.PluginInterfaces.V2;
using VVVV.Utils.VColor;
using VVVV.Utils.VMath;

using VVVV.Core.Logging;
#endregion usings

namespace VVVV.Nodes
{
	#region PluginInfo
	[PluginInfo(Name = "SlideMagnet", Category = "Value", Help = "Basic template with one value in/out", Tags = "")]
	#endregion PluginInfo
	public class ValueSlideMagnetNode : IPluginEvaluate
	{
		#region fields & pins
		[Input("Input", DefaultValue = 1.0)]
		public ISpread<double> input;
		
		[InputAttribute("Speed", IsBang = true)]
		public ISpread<double> speed;
		
		[InputAttribute("Start", IsBang=true)]
		public ISpread<bool> start;
		
		[InputAttribute("End", IsBang = true)]
		public ISpread<bool> end;
		
		[InputAttribute("Distance Threshold", DefaultValue=0.25)]
		public ISpread<double> thr;
		
		[Input("JumpTo", DefaultValue = 1.0)]
		public ISpread<double> jumpto;
		
		[InputAttribute("DoJump", IsBang = true)]
		public ISpread<bool> jump;
		
		[InputAttribute("Reset", IsBang = true)]
		public ISpread<bool> reset;
					
		[InputAttribute("Previous Next", IsBang = true)]
		public ISpread<bool> pn;
		
		[InputAttribute("Enabled")]
		public ISpread<bool> enabled;
		
		private double initial;
		private double current;
		private bool started = false;
		private double ld = 0;

		[Output("Output")]
		public ISpread<double> FOutput;

		[Import()]
		public ILogger FLogger;
		#endregion fields & pins

		//called when data for any output pin is requested
		public void Evaluate(int SpreadMax)
		{
			//Just rest
			if (reset[0])
			{
				current = 0;
				started = false;
				FOutput[0] = current;
				return;
			}
			
			if (jump[0])
			{
				current = jumpto[0];
				started = false;
				FOutput[0] = current;
				return;
			}
			
			if (pn[0] || pn[1])
			{
				if (pn[0])
					current--;
				
				if (pn[1])
					current++;
				
				
				started = false;
				FOutput[0] = current;
				return;
			}
			
			if (enabled[0] == false)
				return;
			
			if (start[0])
			{
				initial = input[0];
				FOutput[0] = current;
				started = true;
				return;
			}
			
			if (end[0])
			{
				started = false;
				if (Math.Abs(ld) > thr[0])
				{
					current += Math.Sign(ld);
					FOutput[0] = current;
				}
				else
				{
					FOutput[0] = current;
				}
				ld = 0;
			}
			
			if (started)
			{
				double c = input[0];
				double diff = c - initial;
				diff = diff * speed[0];
				ld = diff;
				FOutput[0] = current + diff;
			}
			
			/*
			else
			{
				
				double c = input[0];
				
				double diff = c - initial;
				
				current = diff;
			}*/
			
			
		}
	}
}
